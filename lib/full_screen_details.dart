import 'package:flutter/material.dart';
import 'package:myresumeapp/dynamic_widgets.dart/create_text.dart';
import 'package:myresumeapp/utils/colors.dart';
import 'package:myresumeapp/dynamic_widgets.dart/app_bar.dart';

class FullScreenDetails extends StatefulWidget {
  final String title;
  final String body;
  FullScreenDetails({
    @required this.title,
    @required this.body,
    Key key,
  }) : super(key: key);
  @override
  _FullScreenDetailsState createState() => _FullScreenDetailsState();
}

class _FullScreenDetailsState extends State<FullScreenDetails> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: mainAppColor,
        appBar: appBar(),
        body: _fullScreenBody(),
      ),
    );
  }

  Widget _fullScreenBody() {
    return Card(
      elevation: 0,
      color: mainCardColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      margin: EdgeInsets.all(10),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          _fullScreenTitleWidget(),
          _fullScreenBodyWidget(),
        ],
      ),
    );
  }

  Widget _fullScreenTitleWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(11),
          child: createText(
            widget.title,
            size: 30,
            color: mainAppColor,
          ),
        ),
      ],
    );
  }

  Widget _fullScreenBodyWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.all(11),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.79,
              child: createText(
                widget.body,
                size: 20,
                color: mainAppColor,
              ),
            )),
      ],
    );
  }
}
