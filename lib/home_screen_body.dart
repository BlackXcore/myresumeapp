import 'package:flutter/material.dart';
import 'package:myresumeapp/utils/colors.dart';
import 'package:myresumeapp/utils/images.dart';
import 'package:myresumeapp/horizontal_list.dart';
import 'package:myresumeapp/dynamic_widgets.dart/create_text.dart';

class HomeScreenBody extends StatefulWidget {
  @override
  _HomeScreenBodyState createState() => _HomeScreenBodyState();
}

class _HomeScreenBodyState extends State<HomeScreenBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _listOfBodyItems(),
    );
  }

  Widget _listOfBodyItems() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 6,
      itemBuilder: (context, index) {
        return <Widget>[
          SizedBox(
            height: 20,
          ),
          _myResumePicture(),
          _aboutMeSection(),
          _careerTitle(),
          SizedBox(
            height: 10,
          ),
          HorizontalList(),
        ][index];
      },
    );
  }

  Widget _myResumePicture() {
    return GestureDetector(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 10,
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(180),
            ),
            child: Padding(
              padding: EdgeInsets.all(2),
              child: CircleAvatar(
                backgroundImage: AssetImage(
                  myResumeImage,
                ),
                radius: 100,
              ),
            ),
          ),
        ],
      ),
      onTap: _navigateToFullScreenImage,
    );
  }

  void _navigateToFullScreenImage() {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (context) => Hero(
              tag: myResumeImage,
              child: _fullScreenImage(),
            ),
      ),
    );
  }

  Widget _fullScreenImage() {
    return Scaffold(
      backgroundColor: mainAppColor,
      body: Center(
        child: Image.asset(myResumeImage),
      ),
    );
  }

  Widget _aboutMeSection() {
    return Padding(
      padding: EdgeInsets.only(left: 10, top: 10),
      child: createText(
        "Afrika Matshiye",
        size: 30,
        color: mainTextColor,
      ),
    );
  }

  Widget _careerTitle() {
    return Padding(
      padding: EdgeInsets.only(left: 10, top: 1),
      child: createText(
        "- Software Developer",
        size: 18,
        color: mainTextColor,
      ),
    );
  }
}
