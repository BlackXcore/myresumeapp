import 'package:flutter/material.dart';
import 'package:myresumeapp/dynamic_widgets.dart/create_text.dart';
import 'package:myresumeapp/utils/colors.dart';

PreferredSizeWidget appBar() {
  return AppBar(
    title: createText(
      "My Resume",
      size: 30,
    ),
    centerTitle: true,
    elevation: 0,
    iconTheme: IconThemeData(
      color: mainTextColor,
    ),
  );
}
