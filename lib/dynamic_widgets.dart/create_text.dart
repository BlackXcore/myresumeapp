import 'package:flutter/material.dart';
import 'package:myresumeapp/utils/fonts.dart';
import 'package:myresumeapp/utils/colors.dart';

Widget createText(
  String text, {
  @required double size,
  @required Color color,
}) {
  return Text(
    text,
    style: TextStyle(
      color: color,
      fontSize: size,
      fontFamily: quickSand,
      fontWeight: FontWeight.bold,
    ),
    softWrap: true,
  );
}
