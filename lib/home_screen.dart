import 'package:flutter/material.dart';
import 'package:myresumeapp/utils/colors.dart';
import 'package:myresumeapp/home_screen_body.dart';
import 'package:myresumeapp/dynamic_widgets.dart/app_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: mainAppColor,
        appBar: appBar(),
        body: HomeScreenBody(),
        drawer: _drawer(),
      ),
    );
  }

  Theme _drawer() {
    return Theme(
      data: ThemeData.dark(),
      child: Container(),
    );
  }
}
