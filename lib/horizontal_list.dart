import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myresumeapp/full_screen_details.dart';
import 'package:myresumeapp/utils/fonts.dart';
import 'package:myresumeapp/utils/colors.dart';
import 'package:myresumeapp/utils/strings.dart';

class HorizontalList extends StatelessWidget {
  static BuildContext _context;
  final _aboutMe = _createCard(
    title: "About Me",
    body: aboutMeText,
    leftIcon: Icons.account_circle,
    rightIcon: Icons.more_vert,
  );

  final _experience = _createCard(
    title: "Experience",
    body: experienceText,
    leftIcon: Icons.business,
    rightIcon: Icons.more_vert,
  );

  final _education = _createCard(
    title: "Education",
    body: educationText,
    leftIcon: Icons.school,
    rightIcon: Icons.more_vert,
  );

  final _skills = _createCard(
    title: "Technical Skills",
    body: technicalSkillsText,
    leftIcon: Icons.work,
    rightIcon: Icons.more_vert,
  );

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Container(
      width: double.infinity,
      height: 320.0,
      child: ListView.builder(
        itemCount: 4,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return <Widget>[
            _aboutMe,
            _experience,
            _education,
            _skills,
          ][index];
        },
      ),
    );
  }

  static Widget _createCard({
    @required String title,
    @required String body,
    @required IconData leftIcon,
    @required IconData rightIcon,
  }) {
    return GestureDetector(
      child: Card(
        elevation: 8,
        color: mainCardColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        margin: EdgeInsets.all(8),
        child: Container(
          height: 100,
          width: 300,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 5),
              _createCardTitle(
                title,
                leftIcon: leftIcon,
                rightIcon: Icons.arrow_forward,
              ),
              SizedBox(height: 8),
              _createCardBody(body),
            ],
          ),
        ),
      ),
      onTap: () => _navigateToFullScreenDetails(
            title: title,
            body: body,
          ),
    );
  }

  static void _navigateToFullScreenDetails({
    @required String title,
    @required String body,
  }) {
    Navigator.of(_context).push(
      new MaterialPageRoute(
        builder: (_context) => Hero(
              tag: title,
              key: ValueKey(title),
              transitionOnUserGestures: true,
              child: FullScreenDetails(
                title: title,
                body: body,
                key: ValueKey(
                  title,
                ),
              ),
            ),
      ),
    );
  }

  static Widget _createCardTitle(
    String title, {
    @required IconData leftIcon,
    @required IconData rightIcon,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        _createCardIcon(leftIcon),
        _createCardText(
          title,
          30,
        ),
        _createCardIcon(rightIcon),
      ],
    );
  }

  static Widget _createCardIcon(IconData icon) {
    return Card(
      elevation: 0,
      color: mainAppColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(180),
      ),
      child: Icon(
        icon,
        color: mainCardColor,
        size: 30,
      ),
    );
  }

  static Widget _createCardBody(String body) {
    return Padding(
      padding: EdgeInsets.all(11),
      child: _createCardText(
        body,
        17,
      ),
    );
  }

  static Widget _createCardText(String text, double size) {
    return Text(
      text,
      style: TextStyle(
        color: mainAppColor,
        fontSize: size,
        fontFamily: quickSand,
        fontWeight: FontWeight.bold,
      ),
      overflow: TextOverflow.fade,
      maxLines: 10,
      // textAlign: TextAlign.center,
    );
  }
}
