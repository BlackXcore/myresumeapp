import 'package:flutter/material.dart';
import 'package:myresumeapp/home_screen.dart';
import 'package:myresumeapp/utils/colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My Resume App',
      theme: ThemeData(
        primaryColor: mainAppColor,
      ),
      home: MyResumeApp(),
    );
  }
}

class MyResumeApp extends StatefulWidget {
  @override
  _MyResumeAppState createState() => _MyResumeAppState();
}

class _MyResumeAppState extends State<MyResumeApp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: HomeScreen(),
    );
  }
}
