import 'package:flutter/material.dart';

Color mainTextColor = Colors.white;
Color secondaryTextColor = Colors.black;
Color mainAppColor = Color.fromRGBO(30, 40, 56, 1);
Color mainCardColor = Color.fromRGBO(232, 237, 255, 1);
