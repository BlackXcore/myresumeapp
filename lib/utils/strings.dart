//BODY
String aboutMeText =
    "My career started with 4 weeks of bootcamp in the C programming " +
        "language. After that, I used C and PHP for 8 months at WeThinkCode." +
        " Did 4 months of internship at Accenture SA. In my second year at " +
        "WeThinkCode, I studied Game Development in C++ and Unity. I spent my " +
        "last few months there studying mobile development using Google's Flutter " +
        "Framework.";

String educationText = "● WeThinkCode_ - Software Engineering" +
    "\n\nA NPO based in the heart of Johannesburg which aims to revolutionise " +
    "education in South Africa, whose mission is to source, develop and place " +
    "world-class African digital talent globally through a network of " +
    "tuition-free software-engineering schools across the continent" +
    "\n\n● Vumabesala High School - National Senior Certificate" +
    "\n\nSubjects: Accounting, Business Studies, Economics, Math Literacy," +
    "English";

String experienceText = "● Accenture - Software Dev" +
    "\nLearned automation (RPA) using BluePrism, Automation Anywhere and UI Path." +
    "\n\n● Telkom" +
    "\nUsed Postman to test an API and learned basics of Quality Assurance";

String technicalSkillsText = "● Dart (Developed by Google)" +
    "\n● Flutter Framework (Developed by Google)" +
    "\n● JavaScript (Backend, Nodejs)" +
    "\n● C/C++" +
    "\n● C# (Using Unity).";
